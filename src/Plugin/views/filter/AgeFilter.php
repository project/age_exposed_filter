<?php

namespace Drupal\age_exposed_filter\Plugin\views\filter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\views\Plugin\views\filter\NumericFilter;

/**
 * Age filter.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("age_filter")
 */
class AgeFilter extends NumericFilter {

  /**
   * Enable 'extra options' to a user can use listboxes instead of text fields.
   */
  public function hasExtraOptions() {
    return TRUE;
  }

  /**
   * Provide 'extra options' form to configure listboxes instead of text fields.
   *
   * A user can check 'Use listboxes instead of text fields' option that enables
   * to use select elements instead of text fields (for inputting age).
   * Also, the form enables to enter Min and Max value for each of select
   * elements. And based on these values options bill generated automatically.
   */
  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {

    $form['listboxes_config'] = [
      '#type' => 'details',
      '#title' => 'Listboxes settings',
      '#open' => TRUE,
    ];

    $form['listboxes_config']['use_select_instead_of_textfield'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use listboxes instead of text fields'),
      '#default_value' => $this->options['listboxes_config']['use_select_instead_of_textfield'],
    ];

    $form['listboxes_config']['description_1'] = [
      '#type' => 'item',
      '#title' => $this->t('Min and Max values of the Listbox 1 that is using for one value operators like "Is equal to"'),
    ];

    $form['listboxes_config']['select_min_1'] = [
      '#type' => 'number',
      '#title' => $this->t('Min value of the Listbox 1'),
      '#min' => 0,
      '#default_value' => $this->options['listboxes_config']['select_min_1'],
    ];

    $form['listboxes_config']['select_max_1'] = [
      '#type' => 'number',
      '#title' => $this->t('Max value of the Listbox 1'),
      '#min' => 0,
      '#default_value' => $this->options['listboxes_config']['select_max_1'],
    ];

    $form['listboxes_config']['label_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label of the Listbox 1'),
      '#default_value' => $this->options['listboxes_config']['label_1'],
    ];

    $form['listboxes_config']['description_2'] = [
      '#type' => 'item',
      '#title' => $this->t('Min and Max values of the Listbox 2 that is using for the "Is between" and "Is not between" operators'),
    ];

    $form['listboxes_config']['select_min_2'] = [
      '#type' => 'number',
      '#title' => $this->t('Min value of the Listbox 2'),
      '#min' => 0,
      '#default_value' => $this->options['listboxes_config']['select_min_2'],
    ];

    $form['listboxes_config']['select_max_2'] = [
      '#type' => 'number',
      '#title' => $this->t('Max value of the Listbox 2'),
      '#min' => 0,
      '#default_value' => $this->options['listboxes_config']['select_max_2'],
    ];

    $form['listboxes_config']['label_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label of the Listbox 2'),
      '#default_value' => $this->options['listboxes_config']['label_2'],
    ];

    $states = [
      ':input[name="options[listboxes_config][use_select_instead_of_textfield]"]' => ['checked' => TRUE],
    ];

    $form['listboxes_config']['select_min_1']['#states']['enabled'] = $states;
    $form['listboxes_config']['select_max_1']['#states']['enabled'] = $states;
    $form['listboxes_config']['select_min_2']['#states']['enabled'] = $states;
    $form['listboxes_config']['select_max_2']['#states']['enabled'] = $states;
    $form['listboxes_config']['description_1']['#states']['enabled'] = $states;
    $form['listboxes_config']['description_2']['#states']['enabled'] = $states;
    $form['listboxes_config']['label_1']['#states']['enabled'] = $states;
    $form['listboxes_config']['label_2']['#states']['enabled'] = $states;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['listboxes_config']['contains']['use_select_instead_of_textfield'] = ['default' => '0'];
    $options['listboxes_config']['contains']['label_1'] = ['default' => ''];
    $options['listboxes_config']['contains']['select_min_1'] = ['default' => '0'];
    $options['listboxes_config']['contains']['select_max_1'] = ['default' => '0'];
    $options['listboxes_config']['contains']['label_2'] = ['default' => ''];
    $options['listboxes_config']['contains']['select_min_2'] = ['default' => '0'];
    $options['listboxes_config']['contains']['select_max_2'] = ['default' => '0'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultExposeOptions() {
    parent::defaultExposeOptions();

    // If a user checked the 'Use listboxes instead of text fields' option and
    // then will check the 'Expose this filter to visitors, to allow them to
    // change it' option, then all settings will be removed.
    // It happens because the views call defaultExposeOptions() method.
    // To prevent removing of listbox setttings check if they was configured
    // and if yes, then do nothing.
    if ($this->options['listboxes_config']['use_select_instead_of_textfield'] == 1) {
      return;
    }

    $this->options['listboxes_config']['use_select_instead_of_textfield'] = 0;
    $this->options['listboxes_config']['label_1'] = '';
    $this->options['listboxes_config']['select_min_1'] = 0;
    $this->options['listboxes_config']['select_max_1'] = 0;
    $this->options['listboxes_config']['label_2'] = '';
    $this->options['listboxes_config']['select_min_2'] = 0;
    $this->options['listboxes_config']['select_max_2'] = 0;
  }

  /**
   * Provide or a textfield or select elements for inputting age for comparison.
   *
   * If a user checked 'Use listboxes instead of text fields' option, that
   * selects elements will created. In this case, a user will select the age
   * from the listboxes.
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {

    if ($this->options['listboxes_config']['use_select_instead_of_textfield'] == 0) {
      parent::valueForm($form, $form_state);
    }
    else {
      $this->valueFormListboxes($form, $form_state);
    }
  }

  /**
   * Provide select elements instead of textfields.
   */
  protected function valueFormListboxes(&$form, FormStateInterface $form_state) {
    $form['value']['#tree'] = TRUE;

    // We have to make some choices when creating this as an exposed
    // filter form. For example, if the operator is locked and thus
    // not rendered, we can't render dependencies; instead we only
    // render the form items we need.
    $which = 'all';
    if (!empty($form['operator'])) {
      $source = ':input[name="options[operator]"]';
    }
    if ($exposed = $form_state->get('exposed')) {
      $identifier = $this->options['expose']['identifier'];

      if (empty($this->options['expose']['use_operator']) || empty($this->options['expose']['operator_id'])) {
        // Exposed and locked.
        $which = in_array($this->operator, $this->operatorValues(2)) ? 'minmax' : 'value';
      }
      else {
        $source = ':input[name="' . $this->options['expose']['operator_id'] . '"]';
      }
    }
    $user_input = $form_state->getUserInput();
    if ($which == 'all') {
      $form['value']['value'] = $this->createSelectElementForSingleValueOperators();
      // Setup #states for all operators with one value.
      foreach ($this->operatorValues(1) as $operator) {
        $form['value']['value']['#states']['visible'][] = [
          $source => ['value' => $operator],
        ];
      }
      if ($exposed && !isset($user_input[$identifier]['value'])) {
        $user_input[$identifier]['value'] = $this->value['value'];
        $form_state->setUserInput($user_input);
      }
    }
    elseif ($which == 'value') {
      // This is the case when an operator is not exposed
      // and an operation is single.
      $form['value'] = $this->createSelectElementForSingleValueOperators();

      if ($exposed && !isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value['value'];
        $form_state->setUserInput($user_input);
      }
    }

    // Minimum and maximum form fields are associated to some specific operators
    // like 'between'. Ensure that min and max fields are only visible if
    // the associated operator is not excluded from the operator list.
    $two_value_operators_available = ($which == 'all' || $which == 'minmax');

    if (!empty($this->options['expose']['operator_limit_selection']) &&
        !empty($this->options['expose']['operator_list'])) {
      $two_value_operators_available = FALSE;
      foreach ($this->options['expose']['operator_list'] as $operator) {
        if (in_array($operator, $this->operatorValues(2), TRUE)) {
          $two_value_operators_available = TRUE;
          break;
        }
      }
    }

    if ($two_value_operators_available) {

      $this->createSelectElementsForTwoValueOperators($form);

      if ($which == 'all') {
        $states = [];
        // Setup #states for all operators with two values.
        foreach ($this->operatorValues(2) as $operator) {
          $states['#states']['visible'][] = [
            $source => ['value' => $operator],
          ];
        }
        $form['value']['min'] += $states;
        $form['value']['max'] += $states;
      }
      if ($exposed && !isset($user_input[$identifier]['min'])) {
        $user_input[$identifier]['min'] = $this->value['min'];
      }
      if ($exposed && !isset($user_input[$identifier]['max'])) {
        $user_input[$identifier]['max'] = $this->value['max'];
      }

      if (!isset($form['value'])) {
        // Ensure there is something in the 'value'.
        $form['value'] = [
          '#type' => 'value',
          '#value' => NULL,
        ];
      }
    }
  }

  /**
   * Create the select element for equality.
   */
  protected function createSelectElementForSingleValueOperators() {
    $min_1 = $this->options['listboxes_config']['select_min_1'];
    $max_1 = $this->options['listboxes_config']['select_max_1'];
    $options = [];
    // It is important that the first option was like this.
    $options['All'] = $this->t('- Any -');

    for ($i = $min_1; $i <= $max_1; $i++) {
      $options[$i] = $i;
    }

    return [
      '#type' => 'select',
      '#title' => $this->options['listboxes_config']['label_1'],
      '#options' => $options,
      // To prevent the error message: "An illegal choice has been detected.".
      '#validated' => TRUE,
    ];
  }

  /**
   * Create the select elements for equality.
   */
  protected function createSelectElementsForTwoValueOperators(&$form) {

    $min_1 = $this->options['listboxes_config']['select_min_1'];
    $max_1 = $this->options['listboxes_config']['select_max_1'];
    $options = [];
    // It is important that the first option was like this.
    $options['All'] = $this->t('- Any -');

    for ($i = $min_1; $i <= $max_1; $i++) {
      $options[$i] = $i;
    }

    $form['value']['min'] = [
      '#type' => 'select',
      '#title' => $this->options['listboxes_config']['label_1'],
      '#options' => $options,
      // To prevent the error message: "An illegal choice has been detected.".
      '#validated' => TRUE,
    ];

    $min_2 = $this->options['listboxes_config']['select_min_2'];
    $max_2 = $this->options['listboxes_config']['select_max_2'];
    $options = [];
    // It is important that the first option was like this.
    $options['All'] = $this->t('- Any -');

    for ($i = $min_2; $i <= $max_2; $i++) {
      $options[$i] = $i;
    }

    $form['value']['max'] = [
      '#type' => 'select',
      '#title' => $this->options['listboxes_config']['label_2'],
      '#options' => $options,
      // To prevent the error message: "An illegal choice has been detected.".
      '#validated' => TRUE,
    ];
  }

  /**
   * Override parent method to adapt 'between' operations for ages comparison.
   *
   * The initial solution was to pass variables into the addWhereExpression()
   * using 'arguments' array as the third parameter.
   * But it caused the following issue: if we add two 'Age filters'
   * then expression only uses 'arguments' of the first one.
   */
  public function opBetween($field) {
    // When we use select elements and choose '- Any -' that the value is 'All'.
    // When we use textfield that the empty value mean 'All'.
    // Cast it to the same value to make the same comparison for textfields
    // and select elements.
    if ($this->value['min'] == '') {
      $this->value['min'] = 'All';
    }

    if ($this->value['max'] == '') {
      $this->value['max'] = 'All';
    }

    if ($this->value['min'] == 'All' && $this->value['max'] == 'All') {
      if ($this->operator == 'not between') {
        $this->query->addWhereExpression($this->options['group'], 'FALSE');
      }
      return;
    }
    elseif ($this->value['max'] == 'All') {

      $this->operator = $this->operator == 'between' ? '>=' : '<';
      $this->value['value'] = $this->value['min'];
      $this->opSimple($field);
      return;
    }
    elseif ($this->value['min'] == 'All') {

      $this->operator = $this->operator == 'between' ? '<=' : '>';
      $this->value['value'] = $this->value['max'];
      $this->opSimple($field);
      return;
    }
    elseif (!is_numeric($this->value['min']) or !is_numeric($this->value['max'])) {
      $this->query->addWhereExpression($this->options['group'], 'FALSE');
      return;
    }

    $age_min = $this->value['min'];
    $age_max = $this->value['max'];

    $current_date = new DrupalDateTime();
    $current_date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));

    $current_year = $current_date->format('Y');
    $current_month = $current_date->format('m');
    $current_day = $current_date->format('d');

    $birthday_year = $this->query->getDateFormat($field, 'Y', FALSE);
    $birthday_month = $this->query->getDateFormat($field, 'm', FALSE);
    $birthday_day = $this->query->getDateFormat($field, 'd', FALSE);

    $current_year_minus_age_min = ($current_year - $age_min);
    $current_year_minus_age_max = ($current_year - $age_max);
    $current_year_minus_age_min_minus_one = ($current_year - $age_min - 1);
    $current_year_minus_age_max_minus_one = ($current_year - $age_max - 1);
    $current_month = $current_month;
    $current_day = $current_day;

    if ($this->operator == 'between') {

      $exp = '(';
      $exp .= '(';
      $exp .= "($birthday_month < $current_month)";
      $exp .= " AND ($birthday_year <= $current_year_minus_age_min)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month > $current_month)";
      $exp .= " AND ($birthday_year <= $current_year_minus_age_min_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day > $current_day)";
      $exp .= " AND ($birthday_year <= $current_year_minus_age_min_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day <= $current_day)";
      $exp .= " AND ($birthday_year <= $current_year_minus_age_min)";
      $exp .= ')';
      $exp .= ')';

      $exp .= ' AND ';

      $exp .= '(';
      $exp .= '(';
      $exp .= "($birthday_month < $current_month)";
      $exp .= " AND ($birthday_year >= $current_year_minus_age_max)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month > $current_month)";
      $exp .= " AND ($birthday_year >= $current_year_minus_age_max_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day > $current_day)";
      $exp .= " AND ($birthday_year >= $current_year_minus_age_max_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day <= $current_day)";
      $exp .= " AND ($birthday_year >= $current_year_minus_age_max)";
      $exp .= ')';
      $exp .= ')';
    }
    // If ($this->operator == 'not between')
    else {

      $exp = '(';
      $exp .= '(';
      $exp .= "($birthday_month < $current_month)";
      $exp .= " AND ($birthday_year > $current_year_minus_age_min)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month > $current_month)";
      $exp .= " AND ($birthday_year > $current_year_minus_age_min_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day > $current_day)";
      $exp .= " AND ($birthday_year > $current_year_minus_age_min_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day <= $current_day)";
      $exp .= " AND ($birthday_year > $current_year_minus_age_min)";
      $exp .= ')';
      $exp .= ')';

      $exp .= ' OR ';

      $exp .= '(';
      $exp .= '(';
      $exp .= "($birthday_month < $current_month)";
      $exp .= " AND ($birthday_year < $current_year_minus_age_max)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month > $current_month)";
      $exp .= " AND ($birthday_year < $current_year_minus_age_max_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day > $current_day)";
      $exp .= " AND ($birthday_year < $current_year_minus_age_max_minus_one)";
      $exp .= ')';
      $exp .= ' OR ';
      $exp .= '(';
      $exp .= "($birthday_month = $current_month)";
      $exp .= " AND ($birthday_day <= $current_day)";
      $exp .= " AND ($birthday_year < $current_year_minus_age_max)";
      $exp .= ')';
      $exp .= ')';
    }

    $this->query->addWhereExpression($this->options['group'], $exp);
  }

  /**
   * Override parent method to adapt simple operations for ages comparison.
   *
   * The initial solution was to pass variables into the addWhereExpression()
   * using 'arguments' array as the third parameter.
   * But it caused the following issue: if we add two 'Age filters'
   * then expression only uses 'arguments' of the first one.
   */
  public function opSimple($field) {
    // When we use select elements and choose '- Any -' that the value is 'All'.
    // When we use textfield that the empty value mean 'All'.
    // Cast it to the same value to make the same comparison for textfields
    // and select elements.
    // (but it looks like this casting is redundant, because for empty values
    // opSimple() method is not call).
    if ($this->value['value'] == '') {
      $this->value['value'] = 'All';
    }
    // If a user uses listboxes instead of text fields,
    // then it can select '- Any -' option. In this case we display all results
    // just for the '=', '<=', '>=' operations.
    if ($this->value['value'] == 'All') {

      if (!in_array($this->operator, ['=', '<=', '>='])) {
        $this->query->addWhereExpression($this->options['group'], 'FALSE');
      }
      return;
    }
    // If a user use the text fields and input non numeric value,
    // then don't display any results.
    elseif (!is_numeric($this->value['value'])) {
      $this->query->addWhereExpression($this->options['group'], 'FALSE');
      return;
    }

    switch ($this->operator) {
      case '<':
        $operator = '>';
        break;

      case '>':
        $operator = '<';
        break;

      case '<=':
        $operator = '>=';
        break;

      case '>=':
        $operator = '<=';
        break;

      default:
        $operator = $this->operator;
    }

    $age = $this->value['value'];
    $current_date = new DrupalDateTime();
    $current_date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));

    $current_year = $current_date->format('Y');
    $current_month = $current_date->format('m');
    $current_day = $current_date->format('d');

    $birthday_year = $this->query->getDateFormat($field, 'Y', FALSE);
    $birthday_month = $this->query->getDateFormat($field, 'm', FALSE);
    $birthday_day = $this->query->getDateFormat($field, 'd', FALSE);

    $current_year_minus_age = ($current_year - $age);
    $current_year_minus_age_minus_one = ($current_year - $age - 1);
    $current_month = $current_month;
    $current_day = $current_day;

    $exp = '(';
    $exp .= "($birthday_month < $current_month)";
    $exp .= " AND ($birthday_year $operator $current_year_minus_age)";
    $exp .= ')';
    $exp .= ' OR ';
    $exp .= '(';
    $exp .= "($birthday_month > $current_month)";
    $exp .= " AND ($birthday_year $operator $current_year_minus_age_minus_one)";
    $exp .= ')';
    $exp .= ' OR ';
    $exp .= '(';
    $exp .= "($birthday_month = $current_month)";
    $exp .= " AND ($birthday_day > $current_day)";
    $exp .= " AND ($birthday_year $operator $current_year_minus_age_minus_one)";
    $exp .= ')';
    $exp .= ' OR ';
    $exp .= '(';
    $exp .= "($birthday_month = $current_month)";
    $exp .= " AND ($birthday_day <= $current_day)";
    $exp .= " AND ($birthday_year $operator $current_year_minus_age)";
    $exp .= ')';

    $this->query->addWhereExpression($this->options['group'], $exp);
  }

  /**
   * Override the method to exclude the 'regular_expression' operator.
   */
  public function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'opSimple',
        'short' => $this->t('<'),
        'values' => 1,
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('<='),
        'values' => 1,
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'opSimple',
        'short' => $this->t('='),
        'values' => 1,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'opSimple',
        'short' => $this->t('!='),
        'values' => 1,
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('>='),
        'values' => 1,
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'opSimple',
        'short' => $this->t('>'),
        'values' => 1,
      ],
      'between' => [
        'title' => $this->t('Is between'),
        'method' => 'opBetween',
        'short' => $this->t('between'),
        'values' => 2,
      ],
      'not between' => [
        'title' => $this->t('Is not between'),
        'method' => 'opBetween',
        'short' => $this->t('not between'),
        'values' => 2,
      ],
    ];

    // If the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += [
        'empty' => [
          'title' => $this->t('Is empty (NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('empty'),
          'values' => 0,
        ],
        'not empty' => [
          'title' => $this->t('Is not empty (NOT NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('not empty'),
          'values' => 0,
        ],
      ];
    }

    return $operators;
  }

}
