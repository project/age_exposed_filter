# Age exposed filter


## Introduction

The "Age exposed filter" module provides the exposed filter for the Views,
which works on fields of the "Date" type.

Use case
Let's say a user's account has the field of the "Date" type to collect a user's
birthday. And let's assume we need to create a view to display all users that
came of age.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/age_exposed_filter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/age_exposed_filter).

## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module does not require configuration.


## How to Use

Let's say a user's account has the field of the "Date" type to collect a user's
birthday. And let's assume we need to create a view to display all users that
came of age.

To achieve this we need to do the following steps:
1. Create the view to display users.
2. Add to the view the "Age filter (field_birthday)".
3. Select the "Is greater than or equal to" operator.
4. Input 18 into the "Value" field.


## Maintainers

- Andrey Vitushkin (wombatbuddy) - https://www.drupal.org/u/wombatbuddy
