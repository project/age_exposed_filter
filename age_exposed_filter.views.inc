<?php

/**
 * @file
 * Enables 'Age filter' for 'datetime' fields.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function age_exposed_filter_field_views_data_alter(array &$data, FieldStorageConfigInterface $field) {

  if ($field->getType() == 'datetime') {
    $field_name = $field->getName() . '_' . $field->getMainPropertyName();
    foreach ($data as $table_name => $table_data) {

      $reference = $data[$table_name][$field_name];
      $data[$table_name][$field_name . '_age_filter'] = $reference;

      $title = t('Age filter (@field_name)', [
        '@field_name' => $field->getName(),
      ]);

      $data[$table_name][$field_name . '_age_filter']['title'] = $title;
      $data[$table_name][$field_name . '_age_filter']['help'] = t('Provides the age filter.');
      $data[$table_name][$field_name . '_age_filter']['argument']['id'] = 'age_filter';
      $data[$table_name][$field_name . '_age_filter']['filter']['id'] = 'age_filter';
    }
  }
}
